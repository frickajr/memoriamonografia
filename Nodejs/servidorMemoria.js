// Defini��o de Variaveis
var sala = 0;
var qtdeSalas = 5;
var infoSalas = "";

// Defini��o de Arrays
var numeroJogador = [];
var contador = [];
var contRodadas = [];
var arrayDestruirCartas = [];
var arrayCartas = [];
var vezJogador = [];
var intervalID = [];
var intervalTotalID = [];

// Iniciando/Zerando Arrays
for (i = 1; i <= qtdeSalas; i++) {
	numeroJogador[i] = 0;
	contador[i] = 1;
	arrayDestruirCartas[i] = "destruirCartas";
	contRodadas[i] = 1;
	arrayCartas[i] = "";
	vezJogador[i] = 0;
}

// Definindo porta a ser escutada pelo socket.io
var io = require("socket.io").listen(3002);

io.set('log level', 1);
io.sockets.on("connection", function (socket)
{
	// Envia Array de informa��es das 5 salas ( Sala e Qtde Jogador )  
	socket.send(infoSalas);
	console.log('Enviado: ' + infoSalas);
	
	// Dentro dessa chave s�o informa��es trocadas dentro de cada sala.
	socket.on("room", function(sala) {
		
		// Conectando na sala escolhida
        socket.join(sala);
		console.log('Conectado Sala : '+sala);
		
		// Define e envia para o jogador o seu n�mero, tambem � guardado numa var�avel global
		numeroJogador[sala] = contador[sala]++;		
		console.log('Sala : '+sala+' - Conectado Jogador: ' + numeroJogador[sala]);		
		socket.send('numeroJogador,' + numeroJogador[sala]);
		
		// A partir do segundo jogador a logar, ele s� recebe os valores do Array com as posi��es das cartas.
		if (numeroJogador[sala] > 1)
		{	
			socket.send(arrayCartas[sala]);
			console.log('['+sala+'] Enviado para Jogador '+numeroJogador[sala]+' : ' + arrayCartas[sala]);
		}
			
		atualizaInfoSalas();
		
		// Dentro dessa chave s�o recebidas as informa��es trocadas durante o jogo.
		socket.on("message", function (data)
		{
			// Gera um array separando os dados pelas v�rgulas
			var new_data = data.split(',');
			
			// Verifica o primeiro item do array new_data gerado, para saber qual a��o tomar
			switch (new_data[0])
			{
				case 'iniciaJogo':
					socket.broadcast.to(sala).emit("message", 'iniciaJogo');
					console.log('['+sala+'] iniciaJogo !!!');
					break;
					
				case 'arrayCartas':
					// Array contendo as posi��es das cartas j� embaralhadas
					arrayCartas[sala] = data;
					//socket.broadcast.to(sala).emit("message", 'arrayCartas' + arrayCartas[sala]);
					console.log('['+sala+'] Recbido : ' + arrayCartas[sala]);
					break;
					
				case 'cartaVirar':
					socket.broadcast.to(sala).emit("message", 'cartaVirar,' + new_data[1]);
					console.log('['+sala+'] ('+contRodadas[sala]+') Jogador : '+vezJogador[sala]+' - Vira CARTA : ' + new_data[1]);
					break;
					
				case 'cartasDesvirar':
					socket.broadcast.to(sala).emit("message", 'cartasDesvirar,' + new_data[1] + ',' + new_data[2]);
					console.log('['+sala+'] Desvira CARTA: : ' + new_data[1] + ',' + new_data[2]);
					break;
					
				case 'destruirCartas':
					arrayDestruirCartas[sala] += ',' + new_data[1] + ',' + new_data[2];
					socket.broadcast.to(sala).emit("message", arrayDestruirCartas[sala]);
					console.log('['+sala+'] Mandou DestruirCartas: ' + arrayDestruirCartas[sala]);
					break;
					
				case 'vezJogador':
					vezJogador[sala] = new_data[1];
					socket.broadcast.to(sala).emit("message", 'vezJogador,' + vezJogador[sala]);
					console.log('vezJogador,' + vezJogador[sala]);
					break;
					
				case 'sairSala':
					socket.broadcast.to(sala).emit("message", data); // For�a a saida de todos os jogadores da sala
					socket.leave(sala);
					ResetarSala();
					break;
																				
				default:
					break;
			}
			
		});
				
		// Fun��o para resetar a sala e suas informa��es
		function ResetarSala()
		{
			// envia informa��es para todos, incluindo quem mandou.
			io.sockets.in(sala).emit("message",'ResetarSala');
			console.log('['+sala+'] Resetar Sala ');	
			
			contador[sala]=1;
			numeroJogador[sala]=0;
			
			atualizaInfoSalas();
		}
		
		// Fun��o para atualizar e enviar as informa��es das salas  (Qtde Jogador e Jogadores conectados)
		function atualizaInfoSalas() {
			infoSalas = "infoSalas";
			for (i = 1; i <= qtdeSalas; i++)
			{			
				infoSalas += ',' + numeroJogador[i];
			}
			
			socket.broadcast.emit('message', infoSalas);
			console.log('Enviou: ' + infoSalas);
		}
		
	});
});
